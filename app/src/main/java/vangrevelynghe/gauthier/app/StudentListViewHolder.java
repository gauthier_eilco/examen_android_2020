package vangrevelynghe.gauthier.app;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import vangrevelynghe.gauthier.app.Model.Student;

public class StudentListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private List<Student> students;
    private TextView name;
    private TextView email;
    private TextView tel;
    private ImageView photo;

    public StudentListViewHolder(View itemView, List<Student> list) {
        super(itemView);
        this.students = list;
        itemView.setOnClickListener(this);
        photo = itemView.findViewById(R.id.photo);
        name = itemView.findViewById(R.id.name);
        email = itemView.findViewById(R.id.mail);
        tel = itemView.findViewById(R.id.telephone);
    }

    public void display(Student student) {
        name.setText(student.getName().getFirst() + " " + student.getName().getLast());
        email.setText(student.getEmail());
        tel.setText(student.getPhone());
        final int radius = 140;
        final Transformation transformation = new RoundedCornersTransformation(radius, 5);

        if(student.getPicture() != null && !student.getPicture().isEmpty()) {
            Picasso.with(photo.getContext())
                    .load(student.getPicture())
                    .resize(130,130)
                    .transform(transformation)
                    .into(photo);
        }
        else {
            Picasso.with(photo.getContext())
                    .load(R.drawable.unknown_user)
                    .resize(130,130)
                    .transform(transformation)
                    .into(photo);
        }
    }

    @Override
    public void onClick(View v) {
        //Au click sur un item, on ouvre les détails de l'étudiant
        Student clicked = students.get(getLayoutPosition());
        Intent details = new Intent(v.getContext() , DetailsActivity.class);
        details.putExtra("student", clicked);
        v.getContext().startActivity(details);
    }
}