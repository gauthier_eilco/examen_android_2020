package vangrevelynghe.gauthier.app.Model;

import java.io.Serializable;

public class Nationality implements Serializable {
    private String nationality;
    private String flag;
    private String nationnality;

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNationnality() {
        return nationnality;
    }

    public void setNationnality(String nationnality) {
        this.nationnality = nationnality;
    }

}
