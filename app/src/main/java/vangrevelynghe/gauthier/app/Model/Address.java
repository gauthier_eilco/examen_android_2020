package vangrevelynghe.gauthier.app.Model;

import java.io.Serializable;

public class Address implements Serializable {

    private Integer zip;
    private String state;
    private String city;
    private String street;

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return getStreet() + " " + getCity() + " " + getState();
    }
}