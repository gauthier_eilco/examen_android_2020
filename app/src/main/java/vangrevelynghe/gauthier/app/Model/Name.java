package vangrevelynghe.gauthier.app.Model;

import java.io.Serializable;

public class Name implements Serializable {

    private String last;
    private String first;

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

}
