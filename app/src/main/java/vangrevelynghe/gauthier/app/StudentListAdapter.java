package vangrevelynghe.gauthier.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import vangrevelynghe.gauthier.app.Model.Student;


import java.util.List;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListViewHolder> {
    private List<Student> students;

    public StudentListAdapter(List<Student> students) {
        this.students = students;
    }

    @Override
    public StudentListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student, parent, false);
        return new StudentListViewHolder(view, students);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentListViewHolder holder, int position) {
        Student to_display = students.get(position);
        //if(to_display.getPosterPath() != null && !to_display.getPosterPath().equals(""))
        holder.display(to_display);
    }

    @Override
    public int getItemCount() {
        return students.size();
    }
}

