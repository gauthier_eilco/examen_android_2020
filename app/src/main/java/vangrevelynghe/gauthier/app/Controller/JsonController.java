package vangrevelynghe.gauthier.app.Controller;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import vangrevelynghe.gauthier.app.Model.Student;


public interface JsonController {
    @GET("eilco.json")
    Call<List<Student>> getStudents();
}