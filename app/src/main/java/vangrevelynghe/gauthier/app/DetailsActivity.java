package vangrevelynghe.gauthier.app;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import vangrevelynghe.gauthier.app.Model.Student;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.Locale;

public class DetailsActivity extends AppCompatActivity {
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle("Détail");

        //get student
        Intent i = getIntent();
        student = (Student) i.getSerializableExtra("student");
        displayStudent();

        //Modification du header de la page
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //flèche retour de la toolbar
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    public void displayStudent() {
        final int radius = 140;
        Transformation transformation = new RoundedCornersTransformation(radius, 5);
        ImageView image = findViewById(R.id.photo_student);
        ImageView linkedin = findViewById(R.id.linkedin);

        Picasso.with(image.getContext())
                .load(student.getPicture())
                .resize(250, 250)
                .transform(transformation)
                .into(image);

        transformation = new RoundedCornersTransformation(140, 0);
        Picasso.with(linkedin.getContext())
                .load(R.drawable.linkedin)
                .resize(70, 70)
                .transform(transformation)
                .into(linkedin);

        TextView full_name = findViewById(R.id.full_name_student);
        full_name.setText(student.getName().getFirst() + " " + student.getName().getLast());
        TextView tel = findViewById(R.id.telephone_student);
        tel.setText(student.getPhone());

        TextView cp = findViewById(R.id.cp_student);
        TextView ville = findViewById(R.id.ville_student);
        TextView pays = findViewById(R.id.pays_student);
        cp.setText(student.getAddress().getStreet());
        ville.setText(student.getAddress().getCity());
        pays.setText(student.getAddress().getState());

    }

    public void goToLinkedin(View view) {
        String url = student.getLinkedin();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    public void callStudent(View view) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        String num = "";
        if(!student.getPhone().contains("+")) {
            num = "+33" + student.getPhone();
        }
        else {
            num = student.getPhone();
        }
        callIntent.setData(Uri.parse("tel:" + num));
        startActivity(callIntent);


    }

    public void goToMap(View view) {
        String uri = String.format(Locale.ENGLISH, "geo:0,0?q=" + student.getAddress().toString());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }
}
